import bpy
from .utils import *


#=======================================================================================
# NodeGoup List
#=======================================================================================

class BeamNGNodeGroupUilist_actions(bpy.types.Operator):
    bl_idname = "mesh.beamng_nodegroup_list_action"
    bl_label = "NodeGroup List Action"

    action = bpy.props.EnumProperty(
        items=(
            ('UP', "Up", ""),
            ('DOWN', "Down", ""),
            ('REMOVE', "Remove", ""),
            ('ADD', "Add", ""),
        )
    )

    def invoke(self, context, event):
        scene = context.scene
        data = context.active_object.data
        index = data.beamng_active_nodegroup_index

        try:
            item = data.beamng_nodegroups[index]
        except IndexError:
            pass

        else:
            if self.action == 'DOWN' and index < len(data.beamng_nodegroups) - 1:
                item_next = data.beamng_nodegroups[index+1].name
                data.beamng_active_nodegroup_index += 1
                info = 'Nodegroup item %d selected' % (data.beamng_active_nodegroup_index + 1)
                self.report({'INFO'}, info)

            elif self.action == 'UP' and index >= 1:
                item_prev = data.beamng_nodegroups[index-1].name
                data.beamng_active_nodegroup_index -= 1
                info = 'Nodegroup item %d selected' % (data.beamng_active_nodegroup_index + 1)
                self.report({'INFO'}, info)

            elif self.action == 'REMOVE':
                #never delete the default
                if(data.beamng_active_nodegroup_index > 0):
                    info = 'Nodegroup item %s removed from list' % (data.beamng_nodegroups[data.beamng_active_nodegroup_index].name)
                    data.beamng_active_nodegroup_index -= 1
                    self.report({'INFO'}, info)
                    data.beamng_nodegroups.remove(index)

        if self.action == 'ADD':
            item = data.beamng_nodegroups.add()
            existingNodegroupNames = [group.name for group in data.beamng_nodegroups]
            item.name = addNameExtension("NodeGroup", existingNodegroupNames)
            data.beamng_active_nodegroup_index = len(data.beamng_nodegroups) - 1
            info = '%s added to BeamNG Nodegroup list' % (item.name)
            self.report({'INFO'}, info)

        return {"FINISHED"}

# draw
class UL_beamng_nodegroup_items(bpy.types.UIList):
    #how to draw the list items

    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        layout.prop(item, "name", text="", emboss=False, translate=False, icon='VERTEXSEL')

    def invoke(self, context, event):
        pass


#=======================================================================================
# BeamGoup List
#=======================================================================================

class BeamNGBeamGroupUilist_actions(bpy.types.Operator):
    bl_idname = "mesh.beamng_beamgroup_list_action"
    bl_label = "BeamGroup List Action"

    action = bpy.props.EnumProperty(
        items=(
            ('UP', "Up", ""),
            ('DOWN', "Down", ""),
            ('REMOVE', "Remove", ""),
            ('ADD', "Add", ""),
        )
    )

    def invoke(self, context, event):
        scene = context.scene
        data = context.active_object.data
        index = data.beamng_active_beamgroup_index

        try:
            item = data.beamng_beamgroups[index]
        except IndexError:
            pass

        else:
            if self.action == 'DOWN' and index < len(data.beamng_beamgroups) - 1:
                item_next = data.beamng_beamgroups[index+1].name
                data.beamng_active_beamgroup_index += 1
                info = 'Beamgroup item %d selected' % (data.beamng_active_beamgroup_index + 1)
                self.report({'INFO'}, info)

            elif self.action == 'UP' and index >= 1:
                item_prev = data.beamng_beamgroups[index-1].name
                data.beamng_active_beamgroup_index -= 1
                info = 'Beamgroup item %d selected' % (data.beamng_active_beamgroup_index + 1)
                self.report({'INFO'}, info)

            elif self.action == 'REMOVE':
                #never delete the default
                if(data.beamng_active_beamgroup_index > 0):
                    info = 'Beamgroup item %s removed from list' % (data.beamng_beamgroups[data.beamng_active_beamgroup_index].name)
                    data.beamng_active_beamgroup_index -= 1
                    self.report({'INFO'}, info)
                    data.beamng_beamgroups.remove(index)

        if self.action == 'ADD':
            item = data.beamng_beamgroups.add()
            existingBeamgroupNames = [group.name for group in data.beamng_beamgroups]
            item.name = addNameExtension("BeamGroup", existingBeamgroupNames)
            data.beamng_active_beamgroup_index = len(data.beamng_beamgroups) - 1
            info = '%s added to BeamNG Beamgroup list' % (item.name)
            self.report({'INFO'}, info)

        return {"FINISHED"}

# draw
class UL_beamng_beamgroup_items(bpy.types.UIList):
    #how to draw the list items

    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        layout.prop(item, "name", text="", emboss=False, translate=False, icon='EDGESEL')

    def invoke(self, context, event):
        pass


#=======================================================================================
# ColtriGoup List
#=======================================================================================

class BeamNGColtriGroupUilist_actions(bpy.types.Operator):
    bl_idname = "mesh.beamng_coltrigroup_list_action"
    bl_label = "ColtriGroup List Action"

    action = bpy.props.EnumProperty(
        items=(
            ('UP', "Up", ""),
            ('DOWN', "Down", ""),
            ('REMOVE', "Remove", ""),
            ('ADD', "Add", ""),
        )
    )

    def invoke(self, context, event):
        scene = context.scene
        data = context.active_object.data
        index = data.beamng_active_coltrigroup_index

        try:
            item = data.beamng_coltrigroups[index]
        except IndexError:
            pass

        else:
            if self.action == 'DOWN' and index < len(data.beamng_coltrigroups) - 1:
                item_next = data.beamng_coltrigroups[index+1].name
                data.beamng_active_coltrigroup_index += 1
                info = 'Coltrigroup item %d selected' % (data.beamng_active_coltrigroup_index + 1)
                self.report({'INFO'}, info)

            elif self.action == 'UP' and index >= 1:
                item_prev = data.beamng_coltrigroups[index-1].name
                data.beamng_active_coltrigroup_index -= 1
                info = 'Coltrigroup item %d selected' % (data.beamng_active_coltrigroup_index + 1)
                self.report({'INFO'}, info)

            elif self.action == 'REMOVE':
                #never delete the default
                if(data.beamng_active_coltrigroup_index > 0):
                    info = 'Coltrigroup item %s removed from list' % (data.beamng_coltrigroups[data.beamng_active_coltrigroup_index].name)
                    data.beamng_active_coltrigroup_index -= 1
                    self.report({'INFO'}, info)
                    data.beamng_coltrigroups.remove(index)

        if self.action == 'ADD':
            item = data.beamng_coltrigroups.add()
            existingColtrigroupNames = [group.name for group in data.beamng_coltrigroups]
            item.name = addNameExtension("ColtriGroup", existingColtrigroupNames)
            data.beamng_active_coltrigroup_index = len(data.beamng_coltrigroups) - 1
            info = '%s added to BeamNG Coltrigroup list' % (item.name)
            self.report({'INFO'}, info)

        return {"FINISHED"}

# draw
class UL_beamng_coltrigroup_items(bpy.types.UIList):
    #how to draw the list items

    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        layout.prop(item, "name", text="", emboss=False, translate=False, icon='FACESEL')

    def invoke(self, context, event):
        pass