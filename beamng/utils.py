

def addNameExtension(name, listOfExistingNames):
    if(name in listOfExistingNames):
        index = 1
        newName = name + ".00" + str(index)
        while(newName in listOfExistingNames):
            index += 1
            zeros = 3 - len(str(index)) #number of preceding zeros to add
            strIndex = ("0" * zeros) + str(index)
            newName = name + "." + strIndex
        return newName
    return name
    