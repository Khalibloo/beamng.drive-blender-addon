# <pep8 compliant>

bl_info = {
    "name": "BeamNG.drive",
    "version": (1, 0),
    "author": "Khalifa Lame",
    "blender": (2, 77, 0),
    "description": "Tools for modding BeamNG.drive simulation game.",
    "location": "3D Viewport > Properties(N) Panel > BeamNG.drive",
    "category": "Khalibloo"}

import bpy
import bmesh
from . import ops
from . import ui_extras

#============================================================================
# DRAW PANEL
#============================================================================

class BeamNGPanel(bpy.types.Panel):
    """Creates a Panel in the properties context of the 3D viewport"""
    bl_label = "BeamNG.drive"
    bl_idname = "beamng_3d"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'


    def draw(self, context):
        layout = self.layout
        scene = context.scene
        mode = context.mode
        skel_category = context.scene.beamng_skeleton_ui_category
        obj = context.active_object
        layout.operator("object.beamng_import_jbeam_file", text="Import JBeam File")
        if(obj and obj.type != 'MESH'):
            return
        object_type = None
        if(obj is not None):
            object_type = obj.data.beamng_object_type
        
#---------------------------------------------------------------------------------
        #GENERAL TOOLS
        layout.separator()
        col = layout.column(align=True)
        col.label(text="Mark as: ")
        row = col.row(align=True)
        row.operator("object.beamng_mark_mesh_as_flexbody", text="Flexbody")
        row.operator("object.beamng_mark_mesh_as_skeleton", text="Skeleton")
        row.operator("object.beamng_unmark_object", text="Nothing")
        # layout.separator()
        # layout.operator("object.beamng_mesh_to_beam", text="Mesh To Beam")
        # layout.operator("object.beamng_add_beam", text="Add Beam")
        # layout.separator()
        if(object_type == 'SKELETON'):
            if(mode == 'EDIT_MESH'):
                bm = bmesh.from_edit_mesh(obj.data)
                bm.verts.ensure_lookup_table()
                selected_verts = [vert for vert in bm.verts if vert.select]
                selected_edges = [edge for edge in bm.edges if edge.select]
                selected_faces = [face for face in bm.faces if face.select]
            layout.separator()
            col = layout.column(align=True)
            col.label(text="Export JBeam To:")
            row = col.row(align=True)
            row.scale_y = 2
            row.operator("object.beamng_export_jbeam_to_file", text="File")
            row.operator("object.beamng_export_jbeam_to_sim", text="Simulation")
            
            layout.separator()
            #note: the space in text=" " is important if we don't want icons only
            layout.prop(scene, "beamng_skeleton_ui_category", text=" ", expand=True, icon_only=True)

            if(skel_category == 'INFO'):
                layout.prop(obj.data.beamng_general_settings, "name", text="Name")
                layout.prop(obj.data.beamng_general_settings, "authors", text="Authors")
                layout.prop(obj.data.beamng_general_settings, "value", text="Price")

                #layout.separator()
                #layout.label(text="Slots: ")
                #for i in range(0, len(obj.data.beamng_general_settings.slots)):
                #    slot = obj.data.beamng_general_settings.slots[i]
                #    foldIcon = 'TRIA_DOWN'
                #    if(slot.folded):
                #        foldIcon = 'TRIA_RIGHT'
                #    col = layout.column(align=True)
                #    box = col.box()
                #    split = box.row().split(percentage=0.8) #parent row for header box
                #    col1 = split.column()
                #    row = col1.row()
                #    row.operator("mesh.beamng_slot_toggle_fold", text="", icon=foldIcon).index = i
                #    row.prop(slot, "slotType", text="")
                #    col1 = split.column()
                #    col1.operator("mesh.beamng_slot_remove", text="", icon='X').index = i
                #    if(slot.folded == False):
                #        box = col.box() #body box
                #        box.prop(slot, "slotDefault", text="Default: ")
                #        box.prop(slot, "slotDescription", text="Description: ")
                #layout.operator("object.beamng_slot_add", text="Add")

            elif(skel_category == 'SLOTS'):
                layout.prop(obj.data.beamng_general_settings, "slotType", text="Slot")

                layout.separator()
                layout.label(text="Slots: ")
                for i in range(0, len(obj.data.beamng_general_settings.slots)):
                    slot = obj.data.beamng_general_settings.slots[i]
                    foldIcon = 'TRIA_DOWN'
                    if(slot.folded):
                        foldIcon = 'TRIA_RIGHT'
                    col = layout.column(align=True)
                    box = col.box()
                    split = box.row().split(percentage=0.8) #parent row for header box
                    col1 = split.column()
                    row = col1.row()
                    row.operator("mesh.beamng_slot_toggle_fold", text="", icon=foldIcon, emboss=False).index = i
                    row.prop(slot, "type", text="")
                    col1 = split.column()
                    col1.operator("mesh.beamng_slot_remove", text="", icon='X', emboss=False).index = i
                    if(slot.folded == False):
                        box = col.box() #body box
                        box.prop(slot, "default", text="Default: ")
                        box.prop(slot, "description", text="Description: ")
                        col = box.column(align=True)
                        col.label(text="Node Offset:")
                        col.prop(slot, "nodeOffset", text="", index=0)
                        col.prop(slot, "nodeOffset", text="", index=1)
                        col.prop(slot, "nodeOffset", text="", index=2)
                        box.prop(slot, "coreSlot", text="Core Slot")
                layout.operator("object.beamng_slot_add", text="Add", icon='ZOOMIN')

            elif(skel_category == 'PHYSICS'):
                layout.prop(obj.data.beamng_general_settings, "dragCoef")
                layout.prop(obj.data.beamng_general_settings, "scalebeamSpring")
                layout.prop(obj.data.beamng_general_settings, "scalebeamDamp")
                layout.prop(obj.data.beamng_general_settings, "scalebeamDeform")
                layout.prop(obj.data.beamng_general_settings, "scalebeamStrength")

            elif(skel_category == 'CAMERA'):
                layout.prop(obj.data.beamng_general_settings, "distance")
                layout.prop(obj.data.beamng_general_settings, "distanceMin")
                layout.prop(obj.data.beamng_general_settings, "fov")
                col = layout.column(align=True)
                col.label(text="Camera Offset:")
                col.prop(obj.data.beamng_general_settings, "offset", text="", index=0)
                col.prop(obj.data.beamng_general_settings, "offset", text="", index=1)
                col.prop(obj.data.beamng_general_settings, "offset", text="", index=2)

            elif(skel_category == 'NODES'):
                row = layout.row()
                row.template_list("UL_beamng_nodegroup_items", "", obj.data, "beamng_nodegroups", obj.data, "beamng_active_nodegroup_index", rows=3)

                col = row.column(align=True)
                col.operator("mesh.beamng_nodegroup_list_action", icon='ZOOMIN', text="").action = 'ADD'
                col.operator("mesh.beamng_nodegroup_list_action", icon='ZOOMOUT', text="").action = 'REMOVE'
                #col.separator()
                #col.operator("mesh.beamng_nodegroup_list_action", icon='TRIA_UP', text="").action = 'UP'
                #col.operator("mesh.beamng_nodegroup_list_action", icon='TRIA_DOWN', text="").action = 'DOWN'
                
                
                active_nodegroup_index = obj.data.beamng_active_nodegroup_index
                active_nodegroup = obj.data.beamng_nodegroups[active_nodegroup_index]
                if(context.mode == 'EDIT_MESH'):
                    col = layout.column(align=True)
                    row = col.row(align=True)
                    row.operator("mesh.beamng_add_selected_verts_to_nodegroup")
                    row.operator("mesh.beamng_remove_selected_verts_from_nodegroup")
                    row = col.row(align=True)
                    row.operator("mesh.beamng_select_nodegroup_verts", text="Select").action = 'SELECT'
                    row.operator("mesh.beamng_select_nodegroup_verts", text="Deselect").action = 'DESELECT'
                layout.separator()
                col = layout.column(align=True)
                col.label(text="Prefixes: ")
                row = col.row(align=True)
                col = row.column(align=True)
                col.label(text="Right")
                col.prop(active_nodegroup, "rightPrefix", text="")
                col = row.column(align=True)
                col.label(text="Middle")
                col.prop(active_nodegroup, "midPrefix", text="")
                col = row.column(align=True)
                col.label(text="Left")
                col.prop(active_nodegroup, "leftPrefix", text="")

                #layout.separator()
                layout.prop(active_nodegroup, "weight", text="Weight")
                row = layout.row(align=False)
                row.prop(active_nodegroup, "collision", 
                        text="Collision", toggle=False)
                row.prop(active_nodegroup, "selfCollision", 
                        text="Self Collision", toggle=False)
                layout.prop(active_nodegroup, "fixed", text="Position Fixed")
                row = layout.row()
                row.label(text="Material:")
                row.prop(active_nodegroup, "material", text="")
                
                col = layout.column(align=True)
                col.prop(active_nodegroup, "frictionCoef", text="Friction")
                col.prop(active_nodegroup, "surfaceCoef", text="Surface Drag")
                col.prop(active_nodegroup, "volumeCoef", text="Volume Drag")
            
            elif(skel_category == 'BEAMS'):
                row = layout.row()
                row.template_list("UL_beamng_beamgroup_items", "", obj.data, "beamng_beamgroups", obj.data, "beamng_active_beamgroup_index", rows=3)

                col = row.column(align=True)
                col.operator("mesh.beamng_beamgroup_list_action", icon='ZOOMIN', text="").action = 'ADD'
                col.operator("mesh.beamng_beamgroup_list_action", icon='ZOOMOUT', text="").action = 'REMOVE'
                #col.separator()
                #col.operator("mesh.beamng_beamgroup_list_action", icon='TRIA_UP', text="").action = 'UP'
                #col.operator("mesh.beamng_beamgroup_list_action", icon='TRIA_DOWN', text="").action = 'DOWN'
                
                
                active_beamgroup_index = obj.data.beamng_active_beamgroup_index
                active_beamgroup = obj.data.beamng_beamgroups[active_beamgroup_index]
                if(context.mode == 'EDIT_MESH'):
                    col = layout.column(align=True)
                    row = col.row(align=True)
                    row.operator("mesh.beamng_add_selected_edges_to_beamgroup")
                    row.operator("mesh.beamng_remove_selected_edges_from_beamgroup")
                    row = col.row(align=True)
                    row.operator("mesh.beamng_select_beamgroup_edges", text="Select").action = 'SELECT'
                    row.operator("mesh.beamng_select_beamgroup_edges", text="Deselect").action = 'DESELECT'
                layout.separator()
                layout.prop(active_beamgroup, "type", text="", expand=False)
                col = layout.column(align=True)
                col.prop(active_beamgroup, "strength", text="Strength")
                col.prop(active_beamgroup, "spring", text="Spring")
                col.prop(active_beamgroup, "damp", text="Damp")
                col.prop(active_beamgroup, "deform", text="Deform")
                col.prop(active_beamgroup, "precompression", text="Precompression")
                
                if(active_beamgroup.type == 'BEAM_ANISOTROPIC'):
                    col = layout.column(align=True)
                    col.label(text="Anisotropic Beam:")
                    col.prop(active_beamgroup, "springExpansion", text="Spring Expansion")
                    col.prop(active_beamgroup, "dampExpansion", text="Damp Expansion")
                    col.prop(active_beamgroup, "longBound", text="Long Bound")
                    col.prop(active_beamgroup, "transitionZone", text="Transition Zone")
                elif(active_beamgroup.type == 'BEAM_BOUNDED'):
                    col = layout.column(align=True)
                    col.label(text="Bounded Beam:")
                    col.prop(active_beamgroup, "longBound", text="Long Bound")
                    col.prop(active_beamgroup, "shortBound", text="Short Bound")
                    col.prop(active_beamgroup, "limitSpring", text="Limit Spring")
                    col.prop(active_beamgroup, "limitDamp", text="Limit Damp")
                    col.prop(active_beamgroup, "dampFast", text="Damp Fast")
                    col.prop(active_beamgroup, "dampReboundFast", text="Damp Rebound Fast")
                    col.prop(active_beamgroup, "dampVelocitySplit", text="Damp Velocity Split")
                elif(active_beamgroup.type == 'BEAM_SUPPORT'):
                    col = layout.column(align=True)
                    col.label(text="Support Beam:")
                    col.prop(active_beamgroup, "longBound", text="Long Bound")
                elif(active_beamgroup.type == 'BEAM_PRESSURED'):
                    col = layout.column(align=True)
                    col.label(text="Pressured Beam:")
                    col.prop(active_beamgroup, "pressure", text="Pressure")
                    col.prop(active_beamgroup, "surface", text="Surface")
                    col.prop(active_beamgroup, "volumeCoef", text="Volume")
                    col.prop(active_beamgroup, "maxPressure", text="Max Pressure")
                    
                layout.prop(active_beamgroup, "breakGroup", text="BreakGroup")
                layout.prop(active_beamgroup, "breakGroupType", text="BreakGroup Type")
            
            elif(skel_category == 'COLTRIS'):
                row = layout.row()
                row.template_list("UL_beamng_coltrigroup_items", "", 
                                    obj.data, "beamng_coltrigroups", 
                                    obj.data, "beamng_active_coltrigroup_index",
                                    rows=3)

                col = row.column(align=True)
                col.operator("mesh.beamng_coltrigroup_list_action", icon='ZOOMIN', text="").action = 'ADD'
                col.operator("mesh.beamng_coltrigroup_list_action", icon='ZOOMOUT', text="").action = 'REMOVE'
                #col.separator()
                #col.operator("mesh.beamng_coltrigroup_list_action", icon='TRIA_UP', text="").action = 'UP'
                #col.operator("mesh.beamng_coltrigroup_list_action", icon='TRIA_DOWN', text="").action = 'DOWN'
                
                
                active_coltrigroup_index = obj.data.beamng_active_coltrigroup_index
                active_coltrigroup = obj.data.beamng_coltrigroups[active_coltrigroup_index]
                if(context.mode == 'EDIT_MESH'):
                    col = layout.column(align=True)
                    row = col.row(align=True)
                    row.operator("mesh.beamng_add_selected_faces_to_coltrigroup")
                    row.operator("mesh.beamng_remove_selected_faces_from_coltrigroup")
                    row = col.row(align=True)
                    row.operator("mesh.beamng_select_coltrigroup_faces", text="Select").action = 'SELECT'
                    row.operator("mesh.beamng_select_coltrigroup_faces", text="Deselect").action = 'DESELECT'
                layout.separator()
                col = layout.column(align=True)
                col.prop(active_coltrigroup, "dragCoef", text="Drag")
                col.prop(active_coltrigroup, "liftCoef", text="Lift")
                col.prop(active_coltrigroup, "stallAngle", text="Stall Angle")
                layout.prop(active_coltrigroup, "groundModel", text="", expand=False)
                layout.prop(active_coltrigroup, "pressureGroup", text="Pressure Group")
                col = layout.column(align=True)
                col.row(align=True).prop(active_coltrigroup, "pressureUnit", text="Pressure Unit", expand=True)
                if(active_coltrigroup.pressureUnit == 'PSI'):
                    layout.prop(active_coltrigroup, "pressurePSI", text="Pressure")
                else:
                    layout.prop(active_coltrigroup, "pressure", text="Pressure")
                layout.prop(active_coltrigroup, "breakGroup", text="BreakGroup")

def register():
    bpy.utils.register_module(__name__)
    ops.initialize()
    #ops.test()

def unregister():
    ops.destroy()
    bpy.utils.unregister_module(__name__)

if __name__ == "__main__":
    register()
