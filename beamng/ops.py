# <pep8 compliant>

import bpy
import bmesh
import math
import sys
import json
from collections import OrderedDict
from .utils import *
from .parse_utils import *

from bpy_extras.io_utils import ImportHelper, ExportHelper, path_reference_mode
from bpy.props import StringProperty, BoolProperty, EnumProperty

nodeCubeRadius = 0.05
beamBevelDepth = 0.03
nodeMat = None
beamMat = None
coltriMat = None

# pascalToPSI = 0.000145
# psiToPascal = 6894.757

#============================================================================
# DEFINE FUNCTIONS
#============================================================================
    
def meshHasNgon(obj, context):
    for i in range(0, len(obj.data.polygons)):
        if(len(obj.data.polygons[i].vertices) > 4):
            return True
    return False

def meshToBeam(obj, context):
    vCount = len(obj.data.vertices)
    vPos = []
    vNormals = []
    nodes = []
    for i in range(0, vCount):
        v = obj.data.vertices[i]
        vPos.append(obj.matrix_world * v.co)
        vNormals.append(obj.matrix_world * v.normal)
    for i in range(0, vCount):
        bpy.ops.mesh.primitive_cube_add(radius=nodeCubeRadius,
                                        location=vPos[i],
                                        #rotation=vNormals[i]
        )
        nodeCube = context.active_object
        nodes.append(nodeCube)
        nodeCube.name = obj.name + "_" + str(i)
        #material
        bpy.ops.object.material_slot_add()
        nodeCube.material_slots[0].material = nodeMat
        #hook
        bpy.ops.object.select_all(action='DESELECT')
        nodeCube.select = True
        obj.select = True
        context.scene.objects.active = obj
        bpy.ops.object.mode_set(mode='EDIT')
        bm = bmesh.from_edit_mesh(obj.data)
        bpy.ops.mesh.select_all(action='DESELECT')
        bm.verts.ensure_lookup_table()
        bm.verts[i].select = True
        bpy.ops.object.hook_add_selob(use_bone=False)
        bpy.ops.object.mode_set(mode='OBJECT')
        nodeCube.select = False
        
    bpy.ops.object.mode_set(mode='OBJECT')
    context.scene.objects.active = obj
    obj.select = True
    
    #Beams
    for i in range(0, len(obj.data.edges)):
        createBeam(nodes[obj.data.edges[i].vertices[0]], 
                    nodes[obj.data.edges[i].vertices[1]], context)
        bpy.ops.object.select_all(action='DESELECT')
        context.scene.objects.active = obj
        obj.select = True
        
    #Coltris
    obj.show_transparent = True
    #remove all slots
    for i in range(0, len(obj.material_slots)):
        bpy.ops.object.material_slot_remove()
    #assign material
    bpy.ops.object.material_slot_add()
    obj.material_slots[0].material = coltriMat
    
def createBeam(node1, node2, context):
    #Beam material
    global beamMat
    if(beamMat is None):
        beamMat = bpy.data.materials.new("BeamNG Beam")
        beamMat.diffuse_color = (.3, .9, .3)
        beamMat.use_shadeless = True
        #beamMat.use_transparency = True
        #beamMat.alpha = 0.5

    cursorLocBackup = context.scene.cursor_location
    #prep
    bpy.ops.object.mode_set(mode='OBJECT')
    pos1 = node1.matrix_world.to_translation()
    pos2 = node2.matrix_world.to_translation()
    distance = (pos1 - pos2).magnitude
    center = pos1 + ((pos2 - pos1) / 2)
    bpy.ops.object.select_all(action='DESELECT')
    #add nurbs path
    bpy.ops.curve.primitive_nurbs_path_add(location=center)
    beam = context.active_object
    #prep for adding first hook
    node1.select = True
    context.scene.cursor_location = pos1
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.curve.select_all(action='DESELECT')
    #delete unneeded points. we only need 2
    beam.data.splines[0].points[4].select = True
    beam.data.splines[0].points[3].select = True
    beam.data.splines[0].points[2].select = True
    bpy.ops.curve.delete(type='VERT')
    #hook first point
    beam.data.splines[0].points[0].select = True
    bpy.ops.view3d.snap_selected_to_cursor(use_offset=False)
    bpy.ops.object.hook_add_selob()
    #hook second point
    bpy.ops.object.mode_set(mode='OBJECT')
    node1.select = False
    node2.select = True
    context.scene.cursor_location = pos2
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.curve.select_all(action='DESELECT')
    beam.data.splines[0].points[1].select = True
    bpy.ops.view3d.snap_selected_to_cursor(use_offset=False)
    bpy.ops.object.hook_add_selob()
    bpy.ops.curve.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')
    
    #Beam obj props
    bpy.ops.object.select_all(action='DESELECT')
    context.scene.objects.active = beam
    beam.select = True
    beam.name = "BEAM-" + node1.name + "-" + node2.name
    beam.data.fill_mode = 'FULL'
    beam.data.bevel_depth = beamBevelDepth
    beam.data.resolution_u = 2
    #set material
    bpy.ops.object.material_slot_add()
    beam.material_slots[0].material = beamMat
    
    context.scene.cursor_location = cursorLocBackup
    
def markMeshAsSkeleton(obj, context):
    if(obj.type == 'MESH'):
        obj.data.beamng_object_type = 'SKELETON'
        #register vert, edge and face custom properties
        context.scene.objects.active = obj
        bm = bmesh.new()
        bm.from_mesh(obj.data)
        if(bm.verts.layers.string.get("BeamNG NodeGroup") == None):
            bm.verts.layers.string.new("BeamNG NodeGroup")
        if(bm.verts.layers.string.get("BeamNG NodeID") == None):
            bm.verts.layers.string.new("BeamNG NodeID")
        if(bm.edges.layers.string.get("BeamNG BeamGroup") == None):
            bm.edges.layers.string.new("BeamNG BeamGroup")
        if(bm.faces.layers.string.get("BeamNG ColtriGroup") == None):
            bm.faces.layers.string.new("BeamNG ColtriGroup")
        # Finish up, write the bmesh back to the mesh
        bm.to_mesh(obj.data)
        bm.free()  # free and prevent further access
        
        if(len(obj.data.beamng_nodegroups) == 0):
            item = obj.data.beamng_nodegroups.add()
            item.name = "Default NodeGroup"
        if(len(obj.data.beamng_beamgroups) == 0):
            item = obj.data.beamng_beamgroups.add()
            item.name = "Default BeamGroup"
        if(len(obj.data.beamng_coltrigroups) == 0):
            item = obj.data.beamng_coltrigroups.add()
            item.name = "Default ColtriGroup"

        if(len(obj.data.beamng_general_settings.authors) == 0):
            obj.data.beamng_general_settings.authors = context.user_preferences.system.author
    
#============================================================================
# DEFINE OPERATORS
#============================================================================
    
    
class MeshToBeam(bpy.types.Operator):
    """Convert mesh to Beam skeleton"""
    bl_idname = "object.beamng_mesh_to_beam"
    bl_label = "Mesh To Beam"

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (context.active_object.type == 'MESH'))


    def execute(self, context):
        objBackup = bpy.context.active_object
        global nodeMat
        if(nodeMat is None):
            nodeMat = bpy.data.materials.new("BeamNG Node")
            nodeMat.diffuse_color = (0, 0, 1)
            nodeMat.use_shadeless = True
        global coltriMat
        if(coltriMat is None):
            coltriMat = bpy.data.materials.new("BeamNG Coltri")
            coltriMat.diffuse_color = (0, .6, 0)
            coltriMat.use_shadeless = True
            coltriMat.use_transparency = True
            coltriMat.alpha = 0.5

        for obj in bpy.context.selected_objects:
            if(obj.type == 'MESH'):
                bpy.ops.object.mode_set(mode='OBJECT')
                if not meshHasNgon(obj, context):
                    meshToBeam(obj, context)
                else:
                    self.report({'ERROR'}, "Invalid Mesh: 'Mesh has NGons (faces with more than 4 vertices)'")

        bpy.context.scene.objects.active = objBackup
        return {'FINISHED'}
        
class AddBeam (bpy.types.Operator):
    """Create a Beam between the 2 selected Nodes"""
    bl_idname = "object.beamng_add_beam"
    bl_label = "Add Beam"

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (len(context.selected_objects) == 2))
        #and selected objs have node custom property
        
    def execute(self, context):
        createBeam(context.selected_objects[0], context.selected_objects[1], context)
        return {'FINISHED'}

class MarkMeshAsSkeleton (bpy.types.Operator):
    """Register object as a BeamNG skeleton"""
    bl_idname = "object.beamng_mark_mesh_as_skeleton"
    bl_label = "Mark As Skeleton"

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (context.active_object.type == 'MESH')
                and (context.mode == 'OBJECT'))
        
    def execute(self, context):
        objBackup = context.active_object
        for obj in context.selected_objects:
            markMeshAsSkeleton(obj, context)
        context.scene.objects.active = objBackup
        return {'FINISHED'}
        
class MarkMeshAsFlexbody (bpy.types.Operator):
    """Register object as a BeamNG flexbody"""
    bl_idname = "object.beamng_mark_mesh_as_flexbody"
    bl_label = "Mark As Flexbody"

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (context.active_object.type == 'MESH')
                and (context.mode == 'OBJECT'))
        
    def execute(self, context):
        for obj in context.selected_objects:
            if(obj.type == 'MESH'):
                obj.data.beamng_object_type = 'FLEXBODY'
        return {'FINISHED'}
        
class UnmarkObject (bpy.types.Operator):
    """Unregister object as BeamNG skeleton or flexbody"""
    bl_idname = "object.beamng_unmark_object"
    bl_label = "Unmark As Skeleton or Flexbody"

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None)
                and (context.mode == 'OBJECT'))
        
    def execute(self, context):
        for obj in context.selected_objects:
            obj.data.beamng_object_type = 'NONE'
        return {'FINISHED'}

class AddSlot(bpy.types.Operator):
    """Add BeamNG Slot"""
    bl_idname = "object.beamng_slot_add"
    bl_label = "Add Slot"

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (context.active_object.type == 'MESH')
                and (context.active_object.data.beamng_object_type == 'SKELETON'))

    def execute(self, context):
        obj = context.active_object
        item = obj.data.beamng_general_settings.slots.add()
        return {'FINISHED'}

class RemoveSlot(bpy.types.Operator):
    """Remove BeamNG Slot"""
    bl_idname = "mesh.beamng_slot_remove"
    bl_label = "Remove Slot"

    index = bpy.props.IntProperty()

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (context.active_object.type == 'MESH')
                and (context.active_object.data.beamng_object_type == 'SKELETON'))

    def invoke(self, context, event):
        data = context.active_object.data

        try:
            item = data.beamng_general_settings.slots[self.index]
        except IndexError:
            pass

        else:
            data.beamng_general_settings.slots.remove(self.index)
        return {'FINISHED'}

class ToggleFoldSlot(bpy.types.Operator):
    """Fold/Unfold BeamNG Slot"""
    bl_idname = "mesh.beamng_slot_toggle_fold"
    bl_label = "Fold/Unfold Slot"

    index = bpy.props.IntProperty()

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (context.active_object.type == 'MESH')
                and (context.active_object.data.beamng_object_type == 'SKELETON'))

    def invoke(self, context, event):
        data = context.active_object.data
        slot = data.beamng_general_settings.slots[self.index]
        slot.folded = not slot.folded
        return {'FINISHED'}

class ImportJBeamFile(bpy.types.Operator, ImportHelper):
    """Import JBeam file"""
    bl_idname = "object.beamng_import_jbeam_file"
    bl_label = "Import JBeam"
    
    # ImportHelper mixin class uses this
    filename_ext = ".jbeam"

    filter_glob = StringProperty(
            default="*.jbeam",
            options={'HIDDEN'},
            )
    
    path_mode = path_reference_mode
    check_extension = True

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        with open(self.filepath) as f:
            content = "".join(f.readlines())
            inData = json.loads(remove_trailing_commas(remove_comments(content)))
            # TODO: handle case if no keys in inData
            data = inData.get(list(inData.keys())[0], {})
        #create obj to use
        bpy.ops.mesh.primitive_cube_add(location=(0, 0, 0))
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.select_all(action='SELECT')
        bpy.ops.mesh.delete(type='VERT')
        bpy.ops.object.mode_set(mode='OBJECT')
        obj = context.active_object
        markMeshAsSkeleton(obj, context)
        # remove default groups, only use those in import
        obj.data.beamng_nodegroups.remove(0)
        obj.data.beamng_beamgroups.remove(0)
        obj.data.beamng_coltrigroups.remove(0)

        # information
        info = data.get("information", {})
        obj.data.beamng_general_settings.name = info.get("name", "Untitled")
        obj.data.beamng_general_settings.authors = info.get("authors", "")
        obj.data.beamng_general_settings.value = info.get("value", 0)

        # slot type
        obj.data.beamng_general_settings.slotType = data.get("slotType", "main")
        # slots
        slots = data.get("slots")
        if isinstance(slots, list) and len(slots) > 1:
            typeIndex = slots[0].index("type")
            defaultIndex = slots[0].index("default")
            descriptionIndex = slots[0].index("description")
            nodeOffsetIndex = slots[0].index("nodeOffset")
            coreSlotIndex = slots[0].index("coreSlot")
            for i in range(1, len(slots)):
                s = slots[i]
                slot = obj.data.beamng_general_settings.slots.add()
                slot.type = s[typeIndex]
                slot.default = s[defaultIndex]
                slot.description = s[descriptionIndex]
                slot.nodeOffset = s[nodeOffsetIndex]
                slot.coreSlot = s[coreSlotIndex]

        #physics props
        obj.data.beamng_general_settings.dragCoef = data.get("dragCoef", 10)
        obj.data.beamng_general_settings.scalebeamSpring = data.get("scalebeamSpring", 1)
        obj.data.beamng_general_settings.scalebeamDamp = data.get("scalebeamDamp", 1)
        obj.data.beamng_general_settings.scalebeamDeform = data.get("scalebeamDeform", 1)
        obj.data.beamng_general_settings.scalebeamStrength = data.get("scalebeamStrength", 1)

        # refnodes
        # TODO: support refnodes

        #cam external
        camData = data.get("cameraExternal", {})
        camOffset = camData.get("offset", {"x": 0, "y": 0, "z": 0})
        obj.data.beamng_general_settings.distance = camData.get("distance", 10)
        obj.data.beamng_general_settings.distanceMin = camData.get("distanceMin", 10)
        obj.data.beamng_general_settings.offset = [camOffset["x"], camOffset["y"], camOffset["z"]]
        obj.data.beamng_general_settings.fov = camData.get("fov", 65)

        # flexbodies
        # TODO: support flexbodies

        bm = bmesh.new()
        bm.from_mesh(obj.data)
        nodes = data.get("nodes", [])
        nodeIdProp = bm.verts.layers.string.get("BeamNG NodeID")
        nodeGroupProp = bm.verts.layers.string.get("BeamNG NodeGroup")
        beamGroupProp = bm.edges.layers.string.get("BeamNG BeamGroup")
        coltriGroupProp = bm.faces.layers.string.get("BeamNG ColtriGroup")
        beams = data.get("beams", [])
        triangles = data.get("triangles", [])

        verts = [] #store verts to be used in edges and faces
        #states
        nState = {
            "groupName": "Default NodeGroup",
            "nodeWeight": 0,
            "collision": True,
            "selfCollision": False,
            "frictionCoef": 1,
            "nodeMaterial": "RUBBER",
            "fixed": False,
            "surfaceCoef": 0.1,
            "volumeCoef": 0.1,
            "extras": [],
        }

        for n in nodes:
            if(isinstance(n, list) and len(n) == 4 and isinstance(n[0], str) and isinstance(n[1], (float, int)) and
                isinstance(n[2], (float, int)) and isinstance(n[3], (float, int)) and n != nodes[0]):
                #TODO: handle non-unique node names
                v = bm.verts.new((n[1], n[2], n[3]))
                #TODO: check that group is a str
                #TODO: check that node does not have its own group declaration
                v[nodeGroupProp] = nState["groupName"].encode("utf-8")
                v[nodeIdProp] = n[0].encode("utf-8")
                verts.append(v)
            # else if it's a prop declaration
            elif(isinstance(n, dict) and len(n.keys()) > 0):
                for key in n.keys():
                    if key in nState.keys():
                        nState[key] = n[key]
                    if key == "group" and isinstance(n[key], str) and len(n[key].strip()) > 0:
                        # create a new group and assign state values to it
                        group = obj.data.beamng_nodegroups.add()
                        existingNodegroupNames = [g.name for g in obj.data.beamng_nodegroups]
                        group.name = addNameExtension(group.name, existingNodegroupNames)
                        print("group imported: " + group.name)
                        nState["groupName"] = n[key].strip()
                        group.name = n[key].strip()
                        for k in nState.keys():
                            if k in group.keys():
                                group[k] = nState[k]
                        group["weight"] = nState["nodeWeight"]
                        # empty extras to prep for new group values
                        nState["extras"] = []
                    else:
                        # unknown prop. store in extras
                        nState["extras"].append(n[key])
        #states
        bState = {
            "group": "Default BeamGroup",

        }
        for b in beams:
            if(isinstance(b, list) and len(b) == 2 and isinstance(b[0], str) and isinstance(b[1], str) and b != beams[0]):
                v1Filter = [v for v in verts if v[nodeIdProp].decode("utf-8") == b[0]]
                v2Filter = [v for v in verts if v[nodeIdProp].decode("utf-8") == b[1]]
                if(len(v1Filter) == 0 or len(v2Filter) == 0):
                    #TODO: scream error
                    continue
                v1 = v1Filter[0]
                v2 = v2Filter[0]
                try:
                    e = bm.edges.new((v1, v2))
                except:
                    print("Duplicate beam: " + v1[nodeIdProp] + v2[nodeIdProp])
        #states
        cGroup = "Default ColtriGroup"
        for t in triangles:
            if(isinstance(t, list) and len(t) == 3 and isinstance(t[0], str) and isinstance(t[1], str) and
                isinstance(t[2], str) and t != triangles[0]):
                v1Filter = [v for v in verts if v[nodeIdProp].decode("utf-8") == t[0]]
                v2Filter = [v for v in verts if v[nodeIdProp].decode("utf-8") == t[1]]
                v3Filter = [v for v in verts if v[nodeIdProp].decode("utf-8") == t[2]]
                if(len(v1Filter) == 0 or len(v2Filter) == 0 or len(v3Filter) == 0):
                    #TODO: scream error
                    print("Error: unrecognized node name in triangles")
                    continue
                v1 = v1Filter[0]
                v2 = v2Filter[0]
                v3 = v3Filter[0]
                f = bm.faces.new((v1, v2, v3))
        bm.to_mesh(obj.data)
        bm.free()
        # jBeamData.from_json(data)
        return {'FINISHED'}

class ExportJBeamToFile(bpy.types.Operator, ExportHelper):
    """Export JBeam to file"""
    bl_idname = "object.beamng_export_jbeam_to_file"
    bl_label = "Export JBeam"
    
    # ExportHelper mixin class uses this
    filename_ext = ".jbeam"

    filter_glob = StringProperty(
            default="*.jbeam",
            options={'HIDDEN'},
            )
    
    path_mode = path_reference_mode
    check_extension = True

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (context.active_object.type == 'MESH')
                and (context.active_object.data.beamng_object_type == 'SKELETON'))

    def execute(self, context):
        file = open(self.filepath, 'w')
        #generate JBeam data
        data = JBeamData()
        data.package(context)
        file.write(json.dumps(data.to_json(), indent=4))
        file.close()
        return {'FINISHED'}
        
class ExportJBeamToSimulation (bpy.types.Operator):
    """Export JBeam to running instance of BeamNG.drive simulation"""
    bl_idname = "object.beamng_export_jbeam_to_sim"
    bl_label = "Export JBeam"

    @classmethod
    def poll(cls, context):
        return context.active_object is not None
        
    def execute(self, context):
        obj = context.active_object
        #choose file
        #generate jbeam data
        #write jbeam data
        return {'FINISHED'}

class AddSelectedVertsToNodeGroup (bpy.types.Operator):
    """Add selected vertices to active BeamNG nodegroup"""
    bl_idname = "mesh.beamng_add_selected_verts_to_nodegroup"
    bl_label = "Assign"

    @classmethod
    def poll(cls, context):
        return (context.active_object is not None) and (context.mode == 'EDIT_MESH')
        
    def execute(self, context):
        obj = context.active_object
        data = obj.data
        bm = bmesh.from_edit_mesh(obj.data)
        bm.verts.ensure_lookup_table()
        selected_verts = [vert for vert in bm.verts if vert.select]
        active_group = data.beamng_nodegroups[data.beamng_active_nodegroup_index]
        groupProp = bm.verts.layers.string.get("BeamNG NodeGroup")
        for v in selected_verts:
            v[groupProp] = active_group.name.encode("utf-8")
        return {'FINISHED'}
        
class RemoveSelectedVertsFromNodeGroup (bpy.types.Operator):
    """Remove selected vertices from active BeamNG nodegroup. They will be added to default nodegroup"""
    bl_idname = "mesh.beamng_remove_selected_verts_from_nodegroup"
    bl_label = "Remove"

    @classmethod
    def poll(cls, context):
        return (context.active_object is not None) and (context.mode == 'EDIT_MESH')
        
    def execute(self, context):
        obj = context.active_object
        data = obj.data
        bm = bmesh.from_edit_mesh(obj.data)
        bm.verts.ensure_lookup_table()
        selected_verts = [vert for vert in bm.verts if vert.select]
        default_group = data.beamng_nodegroups[0] #assign to default nodegroup
        active_group = data.beamng_nodegroups[data.beamng_active_nodegroup_index]
        groupProp = bm.verts.layers.string.get("BeamNG NodeGroup")
        for v in selected_verts:
            if(v[groupProp].decode("utf-8") == active_group.name):
                v[groupProp] = default_group.name.encode("utf-8")
        return {'FINISHED'}

class AddSelectedEdgesToBeamGroup (bpy.types.Operator):
    """Add selected edges to active BeamNG beamgroup"""
    bl_idname = "mesh.beamng_add_selected_edges_to_beamgroup"
    bl_label = "Assign"

    @classmethod
    def poll(cls, context):
        return (context.active_object is not None) and (context.mode == 'EDIT_MESH')
        
    def execute(self, context):
        obj = context.active_object
        data = obj.data
        bm = bmesh.from_edit_mesh(obj.data)
        bm.edges.ensure_lookup_table()
        selected_edges = [edge for edge in bm.edges if edge.select]
        active_group = data.beamng_beamgroups[data.beamng_active_beamgroup_index]
        groupProp = bm.edges.layers.string.get("BeamNG BeamGroup")
        for e in selected_edges:
            e[groupProp] = active_group.name.encode("utf-8")
        return {'FINISHED'}
        
class RemoveSelectedEdgesFromBeamGroup (bpy.types.Operator):
    """Remove selected edges from active BeamNG beamgroup. They will be added to default beamgroup"""
    bl_idname = "mesh.beamng_remove_selected_edges_from_beamgroup"
    bl_label = "Remove"

    @classmethod
    def poll(cls, context):
        return (context.active_object is not None) and (context.mode == 'EDIT_MESH')
        
    def execute(self, context):
        obj = context.active_object
        data = obj.data
        bm = bmesh.from_edit_mesh(obj.data)
        bm.edges.ensure_lookup_table()
        selected_edges = [edge for edge in bm.edges if edge.select]
        default_group = data.beamng_beamgroups[0] #assign to default beamgroup
        active_group = data.beamng_beamgroups[data.beamng_active_beamgroup_index]
        groupProp = bm.edges.layers.string.get("BeamNG BeamGroup")
        for e in selected_edges:
            if(e[groupProp].decode("utf-8") == active_group.name):
                e[groupProp] = default_group.name.encode("utf-8")
        return {'FINISHED'}
        
class AddSelectedFacesToColtriGroup (bpy.types.Operator):
    """Add selected faces to active BeamNG coltrigroup"""
    bl_idname = "mesh.beamng_add_selected_faces_to_coltrigroup"
    bl_label = "Assign"

    @classmethod
    def poll(cls, context):
        return (context.active_object is not None) and (context.mode == 'EDIT_MESH')
        
    def execute(self, context):
        obj = context.active_object
        data = obj.data
        bm = bmesh.from_edit_mesh(obj.data)
        bm.verts.ensure_lookup_table()
        selected_faces = [face for face in bm.faces if face.select]
        active_group = data.beamng_coltrigroups[data.beamng_active_coltrigroup_index]
        groupProp = bm.faces.layers.string.get("BeamNG ColtriGroup")
        for f in selected_faces:
            f[groupProp] = active_group.name.encode("utf-8")
        return {'FINISHED'}
        
class RemoveSelectedFacesFromColtriGroup (bpy.types.Operator):
    """Remove selected faces from active BeamNG coltrigroup. They will be added to default coltrigroup"""
    bl_idname = "mesh.beamng_remove_selected_faces_from_coltrigroup"
    bl_label = "Remove"

    @classmethod
    def poll(cls, context):
        return (context.active_object is not None) and (context.mode == 'EDIT_MESH')
        
    def execute(self, context):
        obj = context.active_object
        data = obj.data
        bm = bmesh.from_edit_mesh(obj.data)
        bm.faces.ensure_lookup_table()
        selected_faces = [face for face in bm.faces if face.select]
        default_group = data.beamng_coltrigroups[0] #assign to default nodegroup
        active_group = data.beamng_coltrigroups[data.beamng_active_coltrigroup_index]
        groupProp = bm.faces.layers.string.get("BeamNG ColtriGroup")
        for f in selected_faces:
            if(f[groupProp].decode("utf-8") == active_group.name):
                f[groupProp] = default_group.name.encode("utf-8")
        return {'FINISHED'}

class SelectNodeGroupVerts (bpy.types.Operator):
    """Select vertices assigned to active BeamNG nodegroup"""
    bl_idname = "mesh.beamng_select_nodegroup_verts"
    bl_label = "Select"
    
    action = bpy.props.EnumProperty(
        items=(
            ('SELECT', "Select", ""),
            ('DESELECT', "Deselect", "")
        )
    )

    @classmethod
    def poll(cls, context):
        return (context.active_object is not None) and (context.mode == 'EDIT_MESH')
        
    def invoke(self, context, event):
        obj = context.active_object
        bm = bmesh.from_edit_mesh(obj.data)
        bm.verts.ensure_lookup_table()
        active_group = obj.data.beamng_nodegroups[obj.data.beamng_active_nodegroup_index]
        groupProp = bm.verts.layers.string.get("BeamNG NodeGroup")
        isDefaultGroup = False
        if(obj.data.beamng_active_nodegroup_index == 0):
            isDefaultGroup = True
        if (self.action == 'SELECT'):
            print("Select")
            for v in bm.verts:
                if(v[groupProp].decode("utf-8") == active_group.name):
                    v.select = True
                if(isDefaultGroup):
                    if(v[groupProp].decode("utf-8") == ""):
                        v.select = True
        elif (self.action == 'DESELECT'):
            print("Deselect")
            for v in bm.verts:
                if(v[groupProp].decode("utf-8") == active_group.name):
                    v.select = False
                if(isDefaultGroup):
                    if(v[groupProp].decode("utf-8") == ""):
                        v.select = False
        bmesh.update_edit_mesh(obj.data)
        return {'FINISHED'}
        
class SelectBeamGroupEdges (bpy.types.Operator):
    """Select edges assigned to active BeamNG beamgroup"""
    bl_idname = "mesh.beamng_select_beamgroup_edges"
    bl_label = "Select"
    
    action = bpy.props.EnumProperty(
        items=(
            ('SELECT', "Select", ""),
            ('DESELECT', "Deselect", "")
        )
    )

    @classmethod
    def poll(cls, context):
        return (context.active_object is not None) and (context.mode == 'EDIT_MESH')
        
    def invoke(self, context, event):
        obj = context.active_object
        bm = bmesh.from_edit_mesh(obj.data)
        bm.edges.ensure_lookup_table()
        active_group = obj.data.beamng_beamgroups[obj.data.beamng_active_beamgroup_index]
        groupProp = bm.edges.layers.string.get("BeamNG BeamGroup")
        isDefaultGroup = False
        if(obj.data.beamng_active_beamgroup_index == 0):
            isDefaultGroup = True
        if (self.action == 'SELECT'):
            print("Select")
            for e in bm.edges:
                if(e[groupProp].decode("utf-8") == active_group.name):
                    e.select = True
                if(isDefaultGroup):
                    if(e[groupProp].decode("utf-8") == ""):
                        e.select = True
        elif (self.action == 'DESELECT'):
            print("Deselect")
            for e in bm.edges:
                if(e[groupProp].decode("utf-8") == active_group.name):
                    e.select = False
                if(isDefaultGroup):
                    if(e[groupProp].decode("utf-8") == ""):
                        e.select = False
        bmesh.update_edit_mesh(obj.data)
        return {'FINISHED'}
        
class SelectColtriGroupFaces (bpy.types.Operator):
    """Select faces assigned to active BeamNG coltrigroup"""
    bl_idname = "mesh.beamng_select_coltrigroup_faces"
    bl_label = "Select"
    
    action = bpy.props.EnumProperty(
        items=(
            ('SELECT', "Select", ""),
            ('DESELECT', "Deselect", "")
        )
    )

    @classmethod
    def poll(cls, context):
        return (context.active_object is not None) and (context.mode == 'EDIT_MESH')
        
    def invoke(self, context, event):
        obj = context.active_object
        bm = bmesh.from_edit_mesh(obj.data)
        bm.faces.ensure_lookup_table()
        active_group = obj.data.beamng_coltrigroups[obj.data.beamng_active_coltrigroup_index]
        groupProp = bm.faces.layers.string.get("BeamNG ColtriGroup")
        isDefaultGroup = False
        if(obj.data.beamng_active_coltrigroup_index == 0):
            isDefaultGroup = True
        if (self.action == 'SELECT'):
            print("Select")
            for f in bm.faces:
                if(f[groupProp].decode("utf-8") == active_group.name):
                    f.select = True
                if(isDefaultGroup):
                    if(f[groupProp].decode("utf-8") == ""):
                        f.select = True
        elif (self.action == 'DESELECT'):
            print("Deselect")
            for f in bm.faces:
                if(f[groupProp].decode("utf-8") == active_group.name):
                    f.select = False
                if(isDefaultGroup):
                    if(f[groupProp].decode("utf-8") == ""):
                        f.select = False
        bmesh.update_edit_mesh(obj.data)
        return {'FINISHED'}

def get_nodegroup_name(self):
    return self.get("name", "")

def set_nodegroup_name(self, value):
    context = bpy.context
    scene = context.scene
    obj = context.active_object

    wasObjectMode = False
    if(context.mode == 'OBJECT'):
        wasObjectMode = True
    
    bpy.ops.object.mode_set(mode='EDIT')
    bm = bmesh.from_edit_mesh(obj.data)
    oldName = self.name
    print("Old Name: " + oldName)
    newName = value.strip()
    print("New Name: " + newName)
    nodegroupProp = bm.verts.layers.string.get("BeamNG NodeGroup")
    for v in bm.verts:
        if(v[nodegroupProp].decode("utf-8") == oldName):
            v[nodegroupProp] = newName.encode("utf-8")
    if(wasObjectMode):
        bpy.ops.object.mode_set(mode='OBJECT')
    self["name"] = newName

class BeamNGNodeGroup(bpy.types.PropertyGroup):
    name = bpy.props.StringProperty(name="Name", description="NodeGroup Name", 
                                    get=get_nodegroup_name, 
                                    set=set_nodegroup_name)
    midPrefix = bpy.props.StringProperty(name="Middle Prefix", description="NodeGroup Middle Prefix")
    leftPrefix = bpy.props.StringProperty(name="Left Prefix", description="NodeGroup Left Prefix")
    rightPrefix = bpy.props.StringProperty(name="Right Prefix", description="NodeGroup Right Prefix")
    weight = bpy.props.FloatProperty(name="Weight", description="Node Weight")
    collision = bpy.props.BoolProperty(name="Collision", description="Node Collision", 
                                        default=True)
    selfCollision = bpy.props.BoolProperty(name="Self Collision", 
                                            description="Node Self Collision",
                                            default=False)
    group = bpy.props.StringProperty(name="Group", description="Node Group")
    frictionCoef = bpy.props.FloatProperty(name="Friction Coefficient", 
                                            description="NodeGroup Friction Coefficient", 
                                            default=1)
    material = bpy.props.EnumProperty(items =(
                                         ('RUBBER', 'Rubber',''),
                                         ('METAL', 'Metal',''),
                                         ('PLASTIC', 'Plastic',''),
                                         ('GLASS', 'Glass','')
                                         ),
                                name = 'Material',
                                description="NodeGroup physics material",
                                default = 'RUBBER')
    fixed = bpy.props.BoolProperty(name="Fixed", 
                                            description="Node Fixed",
                                            default=False)
    surfaceCoef = bpy.props.FloatProperty(name="Surface Coefficient", 
                                            description="NodeGroup Surface Drag Coefficient", 
                                            default=0.1)
    volumeCoef = bpy.props.FloatProperty(name="Volume Coefficient", 
                                            description="NodeGroup Volume Drag Coefficient", 
                                            default=0.1)
    #pairedNode = bpy.props.StringProperty(name="Paired Node", 
    #                                        description="NodeGroup Paired Node")
    extras = bpy.props.StringProperty(name="Extras", description="NodeGroup Extra Data")
    
    def to_json(self):
        result = {}
        result["name" : self.name]

def get_beamgroup_name(self):
    return self.get("name", "")

def set_beamgroup_name(self, value):
    context = bpy.context
    scene = context.scene
    obj = context.active_object
    
    wasObjectMode = False
    if(context.mode == 'OBJECT'):
        wasObjectMode = True

    bpy.ops.object.mode_set(mode='EDIT')
    bm = bmesh.from_edit_mesh(obj.data)
    oldName = self.name
    newName = value.strip()
    beamgroupProp = bm.edges.layers.string.get("BeamNG BeamGroup")
    for e in bm.edges:
        if(e[beamgroupProp].decode("utf-8") == oldName):
            e[beamgroupProp] = newName.encode("utf-8")
    if(wasObjectMode):
        bpy.ops.object.mode_set(mode='OBJECT')
    self["name"] = newName

class BeamNGBeamGroup(bpy.types.PropertyGroup):
    name = bpy.props.StringProperty(name="Name", description="NodeGroup Name",
                                    get=get_beamgroup_name,
                                    set=set_beamgroup_name)
    type = bpy.props.EnumProperty(items =(
                                         ('NORMAL', 'Normal',''),
                                         ('HYDRO', 'Hydro',''),
                                         ('ANISOTROPIC', 'Anisotropic',''),
                                         ('BOUNDED', 'Bounded',''),
                                         ('PRESSURED', 'Pressured',''),
                                         ('SUPPORT', 'Support',''),
                                         ('BROKEN', 'Broken',''),
                                         ('LBEAM', 'L-Beam','')
                                         ),
                                name = 'Beam Type',
                                description="Type of beam",
                                default = 'NORMAL')
    strength = bpy.props.FloatProperty(name="Strength", description="Beam Strength")
    spring = bpy.props.FloatProperty(name="Spring", description="Beam Spring")
    damp = bpy.props.FloatProperty(name="Damp", description="Beam Damp")
    deform = bpy.props.FloatProperty(name="Deform", description="Beam Deform")
    precompression = bpy.props.FloatProperty(name="Precompression", 
                                            description="Beam Precompression",
                                            default=1)
    breakGroup = bpy.props.StringProperty(name="Break Group", description="Beam Break Group")
    breakGroupType = bpy.props.IntProperty(name="Break Group Type", 
                                            description="Beam Break Group Type",
                                            min=0,
                                            max=1)
    #Anisotropic beams
    springExpansion = bpy.props.FloatProperty(name="Spring Expansion", description="Beam Spring Expansion")
    dampExpansion = bpy.props.FloatProperty(name="Damp Expansion", description="Beam Damp Expansion")
    longBound = bpy.props.FloatProperty(name="Long Bound", 
                                        description="Beam Long Bound",
                                        default=1) #sys.float_info.max
    transitionZone = bpy.props.FloatProperty(name="Transition Zone",
                                            description="Beam Transition Zone", 
                                            default=0)
    #Bounded beams
    #longBound = bpy.props.FloatProperty(name="Long Bound", 
    #                                    description="Beam Long Bound",
    #                                    default=1)
    shortBound = bpy.props.FloatProperty(name="Short Bound", 
                                        description="Beam Short Bound",
                                        default=1)
    limitSpring = bpy.props.FloatProperty(name="Limit Spring", 
                                        description="Beam Limit Spring",
                                        default=1)
    limitDamp = bpy.props.FloatProperty(name="Limit Damp", 
                                        description="Beam Limit Damp",
                                        default=1)
    limitDampRebound = bpy.props.FloatProperty(name="Limit Damp Rebound", 
                                        description="Beam Limit Damp Rebound",
                                        default=1)
    dampRebound = bpy.props.FloatProperty(name="Damp Rebound", 
                                        description="Beam Damp Rebound")
    dampFast = bpy.props.FloatProperty(name="Damp Fast", 
                                        description="Beam Damp Fast")
    dampReboundFast = bpy.props.FloatProperty(name="Damp Rebound Fast", 
                                        description="Beam Damp Rebound Fast")
    dampVelocitySplit = bpy.props.FloatProperty(name="Damp Velocity Split", 
                                        description="Beam Damp Velocity Split")
    
    #Support beam
    #Bounded beams
    #longBound = bpy.props.FloatProperty(name="Long Bound", 
    #                                    description="Beam Long Bound",
    #                                    default=1)
    
    #Pressured beams
    pressure = bpy.props.FloatProperty(name="Pressure", 
                                        description="Beam Pressure")
    surface = bpy.props.FloatProperty(name="Surface", 
                                        description="Beam Surface",
                                        default=1)
    volumeCoef = bpy.props.FloatProperty(name="Volume Coefficient", 
                                        description="Beam Volume Coefficient",
                                        default=1)
    maxPressure = bpy.props.FloatProperty(name="Max Pressure", 
                                        description="Beam Maximum Pressure")
    #L-Beams
   
def get_coltrigroup_name(self):
    return self.get("name", "")

def set_coltrigroup_name(self, value):
    context = bpy.context
    scene = context.scene
    obj = context.active_object

    wasObjectMode = False
    if(context.mode == 'OBJECT'):
        wasObjectMode = True

    bpy.ops.object.mode_set(mode='EDIT')
    bm = bmesh.from_edit_mesh(obj.data)
    oldName = self.name
    newName = value.strip()
    coltrigroupProp = bm.faces.layers.string.get("BeamNG ColtriGroup")
    for f in bm.faces:
        if(f[coltrigroupProp].decode("utf-8") == oldName):
            f[coltrigroupProp] = newName.encode("utf-8")
    if(wasObjectMode):
        bpy.ops.object.mode_set(mode='OBJECT')
    self["name"] = newName
    
class BeamNGColtriGroup(bpy.types.PropertyGroup):
    name = bpy.props.StringProperty(name="Name", description="NodeGroup Name", 
                                    get=get_coltrigroup_name,
                                    set=set_coltrigroup_name)
    dragCoef = bpy.props.FloatProperty(name="Drag Coefficient", 
                                            description="ColtriGroup Drag Coefficient", 
                                            default=10)
    liftCoef = bpy.props.FloatProperty(name="Lift Coefficient", 
                                            description="ColtriGroup Lift Coefficient", 
                                            default=10)
    stallAngle = bpy.props.FloatProperty(name="Stall Angle", 
                                            description="ColtriGroup Stall Angle", 
                                            default=0)
    groundModel = bpy.props.EnumProperty(items =(
                                         ('ASPHALT', 'Asphalt',''),
                                         ('DIRT', 'Dirt',''),
                                         ('ROCK', 'Rock',''),
                                         ('METAL', 'Metal',''),
                                         ('GRASS', 'Grass',''),
                                         ('SAND', 'Sand',''),
                                         ('ROCKYDIRT', 'Rocky Dirt',''),
                                         ('DIRT_GRASS', 'Dirt Grass',''),
                                         ('ICE', 'Ice',''),
                                         ('SNOW', 'Snow',''),
                                         ('MUD', 'Mud',''),
                                         ('KICKPLATE', 'Kickplate','')
                                         ),
                                name = 'Ground Model',
                                description="Ground physics model",
                                default = 'ASPHALT')
    pressureGroup = bpy.props.StringProperty(name="Pressure Group", 
                                            description="Coltri Pressure Group")
    pressurePSI = bpy.props.FloatProperty(name="Pressure PSI", 
                                        description="Coltri Pressure PSI")
    pressure = bpy.props.FloatProperty(name="Pressure Pascal", 
                                        description="Coltri Pressure Pascal")
    pressureUnit = bpy.props.EnumProperty(items =(
                                         ('PASCAL', 'Pascal',''),
                                         ('PSI', 'PSI','')
                                         ),
                                name = 'Pressure Unit',
                                description="Unit of pressure",
                                default = 'PASCAL')
    breakGroup = bpy.props.StringProperty(name="Break Group", 
                                        description="Coltri Break Group")
                                        
class BeamNGSlotGroup(bpy.types.PropertyGroup):
    type = bpy.props.StringProperty(name="Slot Type", description="JBeam Slot Type")
    default = bpy.props.StringProperty(name="Slot Default", description="JBeam Slot Default")
    description = bpy.props.StringProperty(name="Slot Description", description="JBeam Slot Description")
    nodeOffset = bpy.props.FloatVectorProperty(name="Node Offset", 
                                            description="The offset of nodes and flexbodies of this slot")
    coreSlot = bpy.props.BoolProperty(name="Core Slot", description="If enabled, the choice to have the part empty is removed from the part selector/garage")
    folded = bpy.props.BoolProperty(name="Slot Folded", description="JBeam Slot Folded in UI?")

class BeamNGGeneralSettings(bpy.types.PropertyGroup):
    #info
    name = bpy.props.StringProperty(name="Name", description="JBeam Name", default="Untitled")
    authors = bpy.props.StringProperty(name="Authors", description="JBeam Authors", default="")
    value = bpy.props.FloatProperty(name="Value", description="Money Value/Price Of Part")

    #slots
    slotType = bpy.props.StringProperty(name="JBeam Slot Type",
                                        description="JBeam Slot Type",
                                        default="main")
    slots = bpy.props.CollectionProperty(type=BeamNGSlotGroup)

    #physics
    dragCoef = bpy.props.FloatProperty(name="Drag Coefficient", 
                                            description="Drag Coefficient", 
                                            default=10)
    scalebeamSpring = bpy.props.FloatProperty(name="Scale Beam Spring", 
                                            description="Beam Spring Scale Factor", 
                                            default=1)
    scalebeamDamp = bpy.props.FloatProperty(name="Scale Beam Damp", 
                                            description="Beam Damp Scale Factor", 
                                            default=1)
    scalebeamDeform = bpy.props.FloatProperty(name="Scale Beam Deform", 
                                            description="Beam Deform Scale Factor", 
                                            default=1)
    scalebeamStrength = bpy.props.FloatProperty(name="Scale Beam Strength", 
                                            description="Beam Strength Scale Factor", 
                                            default=1)

    #refNodes = 

    #camera
    distance = bpy.props.FloatProperty(name="Camera Distance", 
                                            description="JBeam External Camera Distance", 
                                            default=10)
    distanceMin = bpy.props.FloatProperty(name="Camera Minimum Distance", 
                                            description="JBeam External Camera Minimum Distance", 
                                            default=10)
    offset = bpy.props.FloatVectorProperty(name="Camera Offset", 
                                            description="JBeam External Camera Offset")
    fov = bpy.props.FloatProperty(name="Camera FOV", 
                                            description="JBeam External Camera Field of View", 
                                            min=0,
                                            max=90,
                                            default=65)

    #flexbodies
          
                                      
def initialize():
    #bpy.types.Scene.beamng_skeleton_ui_category = bpy.props.EnumProperty(items =(
    #                                     ('GENERAL', 'General', 'General Settings', 'SCRIPTWIN', 0),
    #                                     ('SKELETON', 'Skeleton', 'Skeleton Settings', 'LATTICE_DATA', 1)
    #                                     ),
    #                            name = 'UI Category',
    #                            description="Skeleton UI category",
    #                            default = 'GENERAL')
    #bpy.types.Scene.beamng_skeleton_general_settings_ui_category = bpy.props.EnumProperty(items =(
    #                                     ('INFO', 'Info', 'Information', 'INFO', 0),
    #                                     ('PHYSICS', 'Phys', 'Physics', 'PHYSICS', 1),
    #                                     ('CAMERA', 'Cam', 'Camera', 'CAMERA_DATA', 2)
    #                                     ),
    #                            name = 'UI Category',
    #                            description="Skeleton General Settings UI category",
    #                            default = 'INFO')
    bpy.types.Scene.beamng_skeleton_ui_category = bpy.props.EnumProperty(items =(
                                         ('INFO', 'Information', 'Information', 'INFO', 0),
                                         ('SLOTS', 'Slots', 'Slots', 'PLUGIN', 1),
                                         ('PHYSICS', 'Physics', 'Physics', 'PHYSICS', 2),
                                         ('CAMERA', 'Camera', 'Camera', 'CAMERA_DATA', 3),
                                         ('NODES', 'Nodes', 'Nodes', 'VERTEXSEL', 4),
                                         ('BEAMS', 'Beams', 'Beams', 'EDGESEL', 5),
                                         ('COLTRIS', 'Coltris', 'Coltris', 'FACESEL', 6)
                                         ),
                                name = 'UI Category',
                                description="Skeleton UI category",
                                default = 'INFO')
    bpy.types.Mesh.beamng_object_type = bpy.props.EnumProperty(items =(
                                         ('NONE', 'None',''),
                                         ('FLEXBODY', 'Flexbody',''),
                                         ('SKELETON', 'Skeleton','')
                                         ),
                                name = 'BeamNG Object Type',
                                description="BeamNG object type",
                                default = 'NONE')
    #nodegroups
    bpy.types.MeshVertex.beamng_nodegroup = bpy.props.StringProperty(
                                                        name="BeamNG NodeGroup",
                                                        description="BeamNG NodeGroup",
                                                        default="Default NodeGroup")
                                                        
    bpy.types.Mesh.beamng_nodegroups = bpy.props.CollectionProperty(type=BeamNGNodeGroup)
    bpy.types.Mesh.beamng_active_nodegroup_index = bpy.props.IntProperty(
                                                name="Active NodeGroup Index", 
                                                description="Active NodeGroup Index",
                                                default=0)
    #beamgroups
    bpy.types.MeshEdge.beamng_beamgroup = bpy.props.StringProperty(
                                                        name="BeamNG BeamGroup",
                                                        description="BeamNG BeamGroup",
                                                        default="Default BeamGroup")
    bpy.types.Mesh.beamng_beamgroups = bpy.props.CollectionProperty(type=BeamNGBeamGroup)
    bpy.types.Mesh.beamng_active_beamgroup_index = bpy.props.IntProperty(
                                                name="Active BeamGroup Index", 
                                                description="Active BeamGroup Index",
                                                default=0)
    #coltrigroups
    bpy.types.MeshPolygon.beamng_coltrigroup = bpy.props.StringProperty(
                                                        name="BeamNG ColtriGroup",
                                                        description="BeamNG ColtriGroup",
                                                        default="Default ColtriGroup")
    bpy.types.Mesh.beamng_coltrigroups = bpy.props.CollectionProperty(type=BeamNGColtriGroup)
    bpy.types.Mesh.beamng_active_coltrigroup_index = bpy.props.IntProperty(
                                                name="Active ColtriGroup Index", 
                                                description="Active ColtriGroup Index",
                                                default=0)
    
    #General
    bpy.types.Mesh.beamng_general_settings = bpy.props.PointerProperty(type=BeamNGGeneralSettings)
    

def destroy():
    del bpy.types.Scene.beamng_category

def to_dict(input_ordered_dict):
    return json.loads(json.dumps(input_ordered_dict))

def to_json(python_object):
    if isinstance(python_object, bytes):
        return {'__class__': 'bytes',
                '__value__': list(python_object)}
    if isinstance(python_object, JBeamCameraExt):
        return {
            'distance': python_object.distance,
            'distanceMin': python_object.distanceMin,
            'offset': python_object.offset,
            'fov': python_object.fov,
        }
    raise TypeError(repr(python_object) + ' is not JSON serializable')
    
class JBeamInfo():
    name = None #JBeam name
    authors = None #Authors names
    value = None #Optional money value/price
    def to_json(self):
        return OrderedDict([
            ('authors', self.authors), 
            ('name', self.name),
            ('value', self.value)
        ])
    def from_json(data):
        self.name = data.get("name", "")
        self.authors = data.get("authors", "")
        self.value = data.get("value", "")
    
class JBeamCameraExt():
    distance = 6 #How far away the default camera position should be
    distanceMin = 4 #How close can it get before it can't go any further
    offset = (0, 0, 0) #Camera offset, X = Left/right, Y = Forward/backwards, Z = Height
    fov = 65
    def to_json(self):
        return OrderedDict([
            ('distance', self.distance),
            ('distanceMin', self.distanceMin),
            ('offset', OrderedDict([("x", self.offset[0]), ("y", self.offset[1]), ("z", self.offset[2])])),
            ('fov', self.fov)
        ])
    def from_json(data):
        self.distance = data.get("distance", 6)
        self.distanceMin = data.get("distanceMin", 4)
        self.offset[0] = data.get("offset", {"x": 0, "y": 0, "z": 0}).get("x", 0)
        self.offset[1] = data.get("offset", {"x": 0, "y": 0, "z": 0}).get("y", 0)
        self.offset[2] = data.get("offset", {"x": 0, "y": 0, "z": 0}).get("z", 0)
        self.fov = data.get("fov", 65)
                
class JBeamData():
    info = JBeamInfo() #JBeamInfo object
    slotType = ""
    slots = []
    
    dragCoef = 10
    scalebeamSpring = 0.84
    scalebeamDamp = 1.3
    scalebeamDeform = 1.5
    scalebeamStrength = 1.78
    
    #refNodes are important, the camera and spawning will work off these, 
    #make sure they are correct, enter false refNodes and your vehicle will not load.
    #The first number should be in the middle of the nodes, this is where the 
    #camera pivots from, if it's off to the side, the camera will be too
    refNodes = [
        # ["ref:", "back:", "left:", "up:", "leftCorner:", "rightCorner:"],
    ]
    
    cameraExternal = JBeamCameraExt() #JBeamCameraExt object
    flexbodies = [
        ["mesh", "[group]:", "nonFlexMaterials"],
        #the first name is the name of the mesh from the .DAE file, 
        #the second name is the group it belongs to
        ["BeamNG_Nodes", ["beamng_frame", "BeamNG_Lowerarm_F", "BeamNG_Upperarm_F"]],
        ["BeamNG_Beams", ["beamng_frame", "BeamNG_Lowerarm_F", "BeamNG_Upperarm_F"]],
        ["BeamNG_XBeams", ["beamng_frame", "BeamNG_Lowerarm_F", "BeamNG_Upperarm_F"]]
    ]
    nodes = [
        ["id", "posX", "posY", "posZ"]#,
        #{"group":""} #The end of the group, always end the last group with a closed bracket
    ]
    beams = [
        ["id1:", "id2:"]#,
    ]
    triangles = [
        ["id1:","id2:","id3:"]#,
    ]

    def package(self, context):
        scene = context.scene
        objBackup = context.active_object
        selectedObjsBackup = context.selected_objects

        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')
        objBackup.select = True
        scene.objects.active = objBackup
        bpy.ops.object.duplicate()
        obj = context.active_object
        #data = obj.to_mesh(scene, apply_modifiers=True, settings='RENDER', calc_tessface=True, calc_undeformed=False)
        data = obj.data
        
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.select_all(action='SELECT')
        bpy.ops.mesh.quads_convert_to_tris(quad_method='SHORTEST_DIAGONAL', ngon_method='BEAUTY')
        bm = bmesh.from_edit_mesh(data)
        nodegroupNames = [group.name for group in data.beamng_nodegroups]
        beamgroupNames = [group.name for group in data.beamng_beamgroups]
        coltrigroupNames = [group.name for group in data.beamng_coltrigroups]

        bm.verts.ensure_lookup_table()
        nodegroupProp = bm.verts.layers.string.get("BeamNG NodeGroup")
        nodeIDProp = bm.verts.layers.string.get("BeamNG NodeID")
        #assign floaters to default group
        for v in bm.verts:
            if(v[nodegroupProp].decode("utf-8") not in nodegroupNames):
                v[nodegroupProp] = data.beamng_nodegroups[0].name.encode("utf-8")

        bm.edges.ensure_lookup_table()
        beamgroupProp = bm.edges.layers.string.get("BeamNG BeamGroup")
        #assign floaters to default group
        for e in bm.edges:
            if(e[beamgroupProp].decode("utf-8") not in beamgroupNames):
                e[beamgroupProp] = data.beamng_beamgroups[0].name.encode("utf-8")

        bm.faces.ensure_lookup_table()
        coltrigroupProp = bm.faces.layers.string.get("BeamNG ColtriGroup")
        #assign floaters to default group
        for f in bm.faces:
            if(f[coltrigroupProp].decode("utf-8") not in coltrigroupNames):
                f[coltrigroupProp] = data.beamng_coltrigroups[0].name.encode("utf-8")

        self.info.name = data.beamng_general_settings.name
        self.info.authors = data.beamng_general_settings.authors
        self.info.value = data.beamng_general_settings.value
        self.slotType = data.beamng_general_settings.slotType
        self.slots = [["type", "default", "description"]]
        for slot in data.beamng_general_settings.slots:
            if(slot.type.strip() != "" and slot.default.strip() != ""):
                s = [
                    slot.type,
                    slot.default,
                    slot.description,
                    {"nodeOffset": {"x": slot.nodeOffset.x, "y": slot.nodeOffset.y, "z": slot.nodeOffset.z}},
                ]
                if slot.coreSlot:
                    s.append({"coreSlot": slot.coreSlot})
                self.slots.append(s)

        self.dragCoef = data.beamng_general_settings.dragCoef
        self.scalebeamSpring = data.beamng_general_settings.scalebeamSpring
        self.scalebeamDamp = data.beamng_general_settings.scalebeamDamp
        self.scalebeamDeform = data.beamng_general_settings.scalebeamDeform
        self.scalebeamStrength = data.beamng_general_settings.scalebeamStrength

        self.refNodes = []
    
        self.cameraExternal.distance = data.beamng_general_settings.distance
        self.cameraExternal.distanceMin = data.beamng_general_settings.distanceMin
        self.cameraExternal.fov = data.beamng_general_settings.fov
        self.cameraExternal.offset = data.beamng_general_settings.offset
        self.flexbodies = [
            ["mesh", "[group]:", "nonFlexMaterials"],
            ["BeamNG_Nodes", ["beamng_frame", "BeamNG_Lowerarm_F", "BeamNG_Upperarm_F"]],
            ["BeamNG_Beams", ["beamng_frame", "BeamNG_Lowerarm_F", "BeamNG_Upperarm_F"]],
            ["BeamNG_XBeams", ["beamng_frame", "BeamNG_Lowerarm_F", "BeamNG_Upperarm_F"]]
        ]
        self.nodes = [
            ["id", "posX", "posY", "posZ"]#,
            #{"group":""} #The end of the group, always end the last group with a closed bracket
        ]
        nodeNames = [] # keep track of used node names
        for i in range(0, len(data.beamng_nodegroups)):
            group = data.beamng_nodegroups[i]
            #default group
            self.nodes.append({"nodeWeight": group.weight})
            self.nodes.append({"collision": group.collision})
            self.nodes.append({"selfCollision": group.selfCollision})
            self.nodes.append({"frictionCoef": group.frictionCoef})
            self.nodes.append({"nodeMaterial": "|NM_" + group.material})
            self.nodes.append({"fixed": group.fixed})
            self.nodes.append({"surfaceCoef": group.surfaceCoef})
            self.nodes.append({"volumeCoef": group.volumeCoef})
            #TODO: pairedNode not yet supported
            self.nodes.append({"group": group.name})
            bm.verts.ensure_lookup_table()
            for v in bm.verts:
                if (v[nodegroupProp] == group.name.encode("utf-8")):
                    if len(v[nodeIDProp]) > 0 and v[nodeIDProp] not in nodeNames:
                        nodeName = v[nodeIDProp]
                    else:
                        nodeName = str(v.index)
                        xPos = v.co.x
                        if(abs(xPos) < 0.001): #roughly 0
                            nodeName = group.midPrefix + nodeName
                        elif(xPos < 0):
                            nodeName = group.rightPrefix + nodeName
                        elif(xPos > 0):
                            nodeName = group.leftPrefix + nodeName
                        v[nodeIDProp] = nodeName.encode("utf-8")
                    self.nodes.append([nodeName, v.co.x, v.co.y, v.co.z])
                    nodeNames.append(nodeName)
        self.nodes.append({"group": ""})


        self.beams = [
            ["id1:", "id2:"]#,
        ]
        for i in range(0, len(data.beamng_beamgroups)):
            group = data.beamng_beamgroups[i]
            #default group
            self.beams.append({"beamSpring": group.spring, "beamDamp": group.damp})
            self.beams.append({"beamDeform": group.deform, "beamStrength": group.strength})
            self.beams.append({"beamPrecompression": group.precompression})
            self.beams.append({"beamType": "|" + group.type})
            #Anisotropic beams
            if(group.type == 'ANISOTROPIC'):
                self.beams.append({"springExpansion": group.springExpansion})
                self.beams.append({"dampExpansion": group.dampExpansion})
                self.beams.append({"beamLongBound": group.longBound})
                self.beams.append({"transitionZone": group.transitionZone})
            #Bounded beams
            if(group.type == 'BOUNDED'):
                self.beams.append({"beamLongBound": group.longBound})
                self.beams.append({"beamShortBound": group.shortBound})
                self.beams.append({"beamLimitSpring": group.limitSpring})
                self.beams.append({"beamLimitDamp": group.limitDamp})
                self.beams.append({"beamLimitDampRebound": group.limitDampRebound})
                self.beams.append({"beamDampRebound": group.dampRebound})
                self.beams.append({"beamDampFast": group.dampFast})
                self.beams.append({"beamDampReboundFast": group.dampReboundFast})
                self.beams.append({"beamDampVelocitySplit": group.dampVelocitySplit})
            #Bounded beams
            if(group.type == 'BOUNDED'):
                self.beams.append({"beamLongBound": group.longBound})
            #Support beams
            if(group.type == 'SUPPORT'):
                self.beams.append({"beamLongBound": group.longBound})
            #Pressured beams
            if(group.type == 'PRESSURED'):
                self.beams.append({"pressure": group.pressure})
                self.beams.append({"surface": group.surface})
                self.beams.append({"volumeCoef": group.volumeCoef})
                self.beams.append({"maxPressure": group.maxPressure})
            #TODO: support L-Beam
            # groupEmpty = True #guilty until proven innocent
            # bm.edges.ensure_lookup_table()
            # for e in bm.edges:
            #     if (e[beamgroupProp] == group.name.encode("utf-8")):
            #         groupEmpty = False
            #         break
            # if(groupEmpty == False):
            self.beams.append({"group": group.name})
            bm.edges.ensure_lookup_table()
            for e in bm.edges:
                if (e[beamgroupProp] == group.name.encode("utf-8")):
                    self.beams.append([e.verts[0][nodeIDProp].decode("utf-8"), 
                                        e.verts[1][nodeIDProp].decode("utf-8")])
        self.beams.append({"group": ""})

        self.triangles = [
            ["id1:","id2:","id3:"]#,
        ]
        for i in range(0, len(data.beamng_coltrigroups)):
            group = data.beamng_coltrigroups[i]
            #default group
            self.triangles.append({"dragCoef": group.dragCoef})
            self.triangles.append({"liftCoef": group.liftCoef})
            self.triangles.append({"stallAngle": group.stallAngle})
            self.triangles.append({"groundModel": group.groundModel})
            if(group.pressureUnit == 'PSI'):
                self.triangles.append({"pressurePSI": group.pressurePSI})
            else:
                self.triangles.append({"pressure": group.pressure})
            #TODO: complete coltri props here
            # groupEmpty = True #guilty until proven innocent
            # bm.faces.ensure_lookup_table()
            # for f in bm.faces:
            #     if (f[coltrigroupProp] == group.name.encode("utf-8")):
            #         groupEmpty = False
            #         break
            # if(groupEmpty == False):
            self.triangles.append({"group": group.name})
            bm.faces.ensure_lookup_table()
            for f in bm.faces:
                if (f[coltrigroupProp] == group.name.encode("utf-8")):
                    self.triangles.append([
                        f.verts[0][nodeIDProp].decode("utf-8"),
                        f.verts[1][nodeIDProp].decode("utf-8"),
                        f.verts[2][nodeIDProp].decode("utf-8")
                    ])
        self.triangles.append({"group": ""})

        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.delete()
        for ob in selectedObjsBackup:
            ob.select = True
        context.scene.objects.active = objBackup

    def to_json(self):
        return OrderedDict([
            ("beamng", OrderedDict([
                    ("information", self.info.to_json()),
                    ("slotType", self.slotType),
                    ("slots", self.slots),

                    ("dragCoef", self.dragCoef),
	                ("scalebeamSpring", self.scalebeamSpring),
	                ("scalebeamDamp", self.scalebeamDamp),
	                ("scalebeamDeform", self.scalebeamDeform),
	                ("scalebeamStrength", self.scalebeamStrength),

                    ("refNodes", self.refNodes),
                    ("cameraExternal", self.cameraExternal.to_json()),

                    ("flexbodies", self.flexbodies),
                    ("nodes", self.nodes),
                    ("beams", self.beams),
                    ("triangles", self.triangles)
                ])
            )
        ])
    
    def from_json(data):
        self.information.authors

def test():
    obj = JBeamData()
    print(json.dumps(obj.to_json(), indent=4))


