# Introduction #

This addon helps simplify the process of building a BeamNG Drive physics skeleton file. Simply select a mesh and use the addon's UI on Blender's N-Panel to set up your options. Once done, click the export button to save your jbeam file.

The addon was developed a while back as a demo for a client who canceled. I've now decided to release it with the hope that others may find it useful and perhaps even support its development. Head over to Patreon https://www.patreon.com/khalibloo if you'd like to support it.

# Installation #
The installation process is the same as most Blender addons. Simply download and extract the zip file from [https://khalibloo.itch.io/beamng-drive-blender-addon](https://khalibloo.itch.io/beamng-drive-blender-addon) and copy the "beamng" folder to your Blender addons folder. Then enable it in Blender's user preferences window under Khalibloo>BeamNG and save your user settings. This project is currently undergoing testing. Please report any issues you come across.
